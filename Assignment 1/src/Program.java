import java.sql.Time;
import java.util.Date;

public class Program {
    public static void main(String[] args) {
        //insert du lieu
        // Department
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "SUPPORT";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "SALE";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "SERVICES";
        Department[] departments = {department1, department2, department3};
        // Position
        Position position1 = new Position();
        position1.positionID = 1;
        position1.positionName = "DEV";

        Position position2 = new Position();
        position2.positionID = 2;
        position2.positionName = "TEST";

        Position position3 = new Position();
        position3.positionID = 3;
        position3.positionName = "PM";
        // Account
        Account account1 = new Account();
        account1.accountId = 1;
        account1.email = "a1.gmail.com";
        account1.userName = "user1";
        account1.fullName = "full1";
        account1.department = department1;
        account1.position = position1;
        account1.createDate = new Date();

        Account account2 = new Account();
        account2.accountId = 2;
        account2.email = "a2.gmail.com";
        account2.userName = "user2";
        account2.fullName = "full2";
        account2.department = department2;
        account2.position = position2;
        account2.createDate = new Date();

        Account account3 = new Account();
        account3.accountId = 3;
        account3.email = "a3.mail.com";
        account3.userName = "user3";
        account3.fullName = "full3";
        account3.department = department3;
        account3.position = position3;
        account3.createDate = new Date();
        //dsAccount
        Account[] accounts1 = {account1, account2, account3};
        //Group

        Group group1 = new Group();
        group1.groupId = 1;
        group1.groupName = "Group1";
        group1.creator = account1;
        group1.createDate = new Date();

        Group group2 = new Group();
        group2.groupId = 2;
        group2.groupName = "Group2";
        group2.creator = account2;
        group2.createDate = new Date();

        Group group3 = new Group();
        group3.groupId = 3;
        group3.groupName = "Group3";
        group3.creator = account3;
        group3.createDate = new Date();
        //gan dsgroup1
        Group[] dsGroup1 = {group1, group2, group3} ;
        account2.groups = dsGroup1;
        //GroupAccount
        GroupAccount groupAccount1 = new GroupAccount();
        groupAccount1.group = group1;
        groupAccount1.account = account1;
        groupAccount1.joinDate = new Date();

        GroupAccount groupAccount2 = new GroupAccount();
        groupAccount2.group = group2;
        groupAccount2.account = account2;
        groupAccount2.joinDate = new Date();

        GroupAccount groupAccount3 = new GroupAccount();
        groupAccount3.group = group3;
        groupAccount3.account = account3;
        groupAccount3.joinDate = new Date();
        //TypeQuestion
        TypeQuestion typeQuestion1 = new TypeQuestion();
        typeQuestion1.typeId = 1;

        TypeQuestion typeQuestion2 = new TypeQuestion();
        typeQuestion2.typeId = 2;

        TypeQuestion typeQuestion3 = new TypeQuestion();
        typeQuestion3.typeId = 3;
        //CategoryQuestion
        CategoryQuestion categoryQuestion1 = new CategoryQuestion();
        categoryQuestion1.categoryId = 1;
        categoryQuestion1.categoryName = "JAVA";

        CategoryQuestion categoryQuestion2 = new CategoryQuestion();
        categoryQuestion2.categoryId = 2;
        categoryQuestion2.categoryName = "SQL";

        CategoryQuestion categoryQuestion3 = new CategoryQuestion();
        categoryQuestion3.categoryId = 3;
        categoryQuestion3.categoryName = ".NET";
        //Question
        Question question1 = new Question();
        question1.questionId = 1;
        question1.content = "QUESTION1";
        question1.category = categoryQuestion1;
        question1.type = typeQuestion1;
        question1.creator = account1;
        question1.createDate = new Date();

        Question question2 = new Question();
        question2.questionId = 2;
        question2.content = "QUESTION2";
        question2.category = categoryQuestion2;
        question2.type = typeQuestion2;
        question2.creator = account2;
        question2.createDate = new Date();

        Question question3 = new Question();
        question3.questionId = 3;
        question3.content = "QUESTION3";
        question3.category = categoryQuestion3;
        question3.type = typeQuestion3;
        question3.creator = account3;
        question3.createDate = new Date();
        //  Answer
        Answer answer1 = new Answer();
        answer1.AnswerId = 1;
        answer1.content = "ANSWER1";
        answer1.question = question1;
        answer1.isCorrect = true;

        Answer answer2 = new Answer();
        answer2.AnswerId = 2;
        answer2.content = "ANSWER2";
        answer2.question = question2;
        answer2.isCorrect = true;

        Answer answer3 = new Answer();
        answer3.AnswerId = 3;
        answer3.content = "ANSWER3";
        answer3.question = question3;
        answer3.isCorrect = false;
        // Exam
        Exam exam1 = new Exam();
        exam1.examId = 1;
        exam1.code = 11;
        exam1.title = "Exam1";
        exam1.category = categoryQuestion1;
        exam1.duration = new Time(60);
        exam1.creator = account1;
        exam1.createDate = new Date();

        Exam exam2 = new Exam();
        exam2.examId = 2;
        exam2.code = 22;
        exam2.title = "Exam2";
        exam2.category = categoryQuestion2;
        exam2.duration = new Time(60);
        exam2.creator = account2;
        exam2.createDate = new Date();

        Exam exam3 = new Exam();
        exam3.examId = 3;
        exam3.code = 33;
        exam3.title = "Exam3";
        exam3.category = categoryQuestion3;
        exam3.duration = new Time(60);
        exam3.creator = account3;
        exam3.createDate = new Date();
        // ExamQuestion
        ExamQuestion examQuestion1 = new ExamQuestion();
        examQuestion1.exam = exam1;
        examQuestion1.question = question1;

        ExamQuestion examQuestion2 = new ExamQuestion();
        examQuestion2.exam = exam2;
        examQuestion2.question = question2;

        ExamQuestion examQuestion3 = new ExamQuestion();
        examQuestion3.exam = exam3;
        examQuestion3.question = question3;
        //Question 3:hãy in ("Thong tin cua Account1");
        System.out.println("ID:" + account1.accountId);
        System.out.println("Email:" + account1.email);
        System.out.println("UserName:" + account1.userName);
        System.out.println("FullName:" + account1.fullName);
        System.out.println("Department:" + account1.department.departmentName);
        System.out.println("Position:" + account1.position.positionName);
        System.out.println("CreateDate:"+account1.createDate);

        //lam bai tap Exercise1
            //Question1
        //Exercise1 exercise1 = new Exercise1();
        //exercise1.question1(account2);
            //Question2
        //Exercise1 exercise1 = new Exercise1();
        //exercise1.question2(account2);
        Exercise1 exercise1 = new Exercise1();
        exercise1.question1(account2);
        exercise1.question2(account2);
        exercise1.question3(account2);
        exercise1.question4(account1);
        exercise1.question5(accounts1);
        exercise1.question6(dsGroup1);
        exercise1.question7(account1);
        exercise1.question8(accounts1);
        exercise1.question9(departments);
        exercise1.question10(accounts1);
        exercise1.question11(departments);
        exercise1.question12(departments);
        exercise1.question13(accounts1);
        exercise1.question14(accounts1);
        exercise1.question15();
        exercise1.question16(accounts1);
    }
}
        